package com.justyna.epam.project.sortingApp;

import org.junit.Test;

public class ZeroArgumentCaseTest {

    @Test (expected = NumberFormatException.class)
    public void EmptyStringCaseTest() {
        SortingApp app = new SortingApp();
        app.sort("");
    }

    @Test (expected = NumberFormatException.class)
    public void WhiteSpaceCaseTest() {
        SortingApp app = new SortingApp();
        app.sort("    ");
    }

    @Test (expected = NumberFormatException.class)
    public void InvalidInputFormatCaseTest() {
        SortingApp app = new SortingApp();
        app.sort("just words");
    }
}