package com.justyna.epam.project.sortingApp;

import org.junit.Test;

public class MoreThanTenArgumentsCaseTest {

    @Test (expected = IndexOutOfBoundsException.class)
    public void MoreThanTenArgumentsCaseTest() {
        SortingApp app = new SortingApp();
        String tooMuchArguments = " 2 4 8 7 9 10 89 1 3 0 785";
        app.sort(tooMuchArguments);
    }
}
