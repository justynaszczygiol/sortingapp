package com.justyna.epam.project.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TenArgumentsCaseTest {

    SortingApp app = new SortingApp();
    private String input;
    private String expected;

    public TenArgumentsCaseTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"2 4 8 7 9 10 89 1 3 0", "[0, 1, 2, 3, 4, 7, 8, 9, 10, 89]"},
                {" 10 9 8 7 6 5 4 3 2 1 ", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"},
                {" 8 -10 33 1456 -1486 18 0 15 4 111", "[-1486, -10, 0, 4, 8, 15, 18, 33, 111, 1456]"},
                {"-8 -7 -6 -3 -2 -8 -5 -1 -7 -8", "[-8, -8, -8, -7, -7, -6, -5, -3, -2, -1]"},
                {"1 1 1 1 1 1 1 1 1 1", "[1, 1, 1, 1, 1, 1, 1, 1, 1, 1]"},
                {"0 1 2 3 4 5 6 7 8 9 ", "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"}
        });
    }

    @Test
    public void tenArgumentsCaseTest() {
        assertEquals(expected, app.sort(input));
    }
}