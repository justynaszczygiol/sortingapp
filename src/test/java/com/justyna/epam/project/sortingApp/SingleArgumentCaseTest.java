package com.justyna.epam.project.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SingleArgumentCaseTest {

    SortingApp app = new SortingApp();
    private String input;
    private String expected;

    public SingleArgumentCaseTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"-500", "[-500]"},
                {" 0 ", "[0]"},
                {" 1", "[1]"}
        });
    }

    @Test
    public void tenArgumentsCaseTest() {
        assertEquals(expected, app.sort(input));
    }
}