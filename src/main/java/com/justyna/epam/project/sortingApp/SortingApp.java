package com.justyna.epam.project.sortingApp;

import java.util.Arrays;

public class SortingApp {

    public String sort(String values) throws  NumberFormatException, IndexOutOfBoundsException {
        int[] intValues;
                try {
            intValues = parseToInt(values);
            Arrays.sort(intValues);
            System.out.println(Arrays.toString(intValues));
        } catch (NumberFormatException e) {
                    throw new NumberFormatException();
        } catch (IndexOutOfBoundsException e) {
                    throw new IndexOutOfBoundsException();
        }
        return Arrays.toString(intValues);
    }

    public int[] parseToInt(String values) throws NumberFormatException, IndexOutOfBoundsException {
        String[] stringValues = values.trim().split(" ");
        int[] intValues = new int[stringValues.length];

        if (intValues.length == 0) {
            throw new NumberFormatException();
        } else if (intValues.length <= 10) {
            for (int i = 0; i < intValues.length; i++) {
                int value = Integer.valueOf(stringValues[i]);
                intValues[i] = value;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
        return intValues;
    }
}